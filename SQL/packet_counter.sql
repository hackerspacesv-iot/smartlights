-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2019 at 03:38 PM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sensors`
--

-- --------------------------------------------------------

--
-- Table structure for table `packet_counter`
--

CREATE TABLE `packet_counter` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `http_packets` bigint(20) NOT NULL,
  `ssl_packets` bigint(20) NOT NULL DEFAULT '0',
  `captured` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ipv6_http` bigint(20) NOT NULL DEFAULT '0',
  `ipv6_https` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `packet_counter`
--
ALTER TABLE `packet_counter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `captured` (`captured`),
  ADD KEY `ssl_packets` (`ssl_packets`),
  ADD KEY `http_packets` (`http_packets`),
  ADD KEY `ssl_packets_2` (`ssl_packets`),
  ADD KEY `captured_2` (`captured`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `packet_counter`
--
ALTER TABLE `packet_counter`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
