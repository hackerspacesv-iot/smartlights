# Archivos SQL
Esta carpeta contiene los archivos SQL con la estructura de las
tablas utilizadas para almacenar los datos de sensores.

Para este proyecto se almacenaron los datos en MariaDB 10.0.38

Se incluyen dos archivos de definición de estructura:

1. light_sensor.sql: que contiene el archivo de definición para la
tabla light_sensor.
2. switch_sensor.sql: que contiene el archivo de definciión para
la tabla switch_sensor.
