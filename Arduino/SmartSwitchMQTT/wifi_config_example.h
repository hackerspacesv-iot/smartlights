// Ingresa la información de tu red inalambrica
// y renombra este archivo a "wifi_config.h"

#ifndef STASSID
#define STASSID "AP_SSID"
#define STAPSK  "1234567890"
#endif

// Definir la dirección del servidor MQTT entre comillas
#define MQTT_SERVER "1.2.3.4"
