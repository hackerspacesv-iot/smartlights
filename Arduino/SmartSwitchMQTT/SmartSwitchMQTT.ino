/*
 *******************************************************************************
 *
 * Purpose: Example of using the Arduino MqttClient with Esp8266WiFiClient.
 * Project URL: https://github.com/monstrenyatko/ArduinoMqtt
 *
 *******************************************************************************
 * Copyright Oleg Kovalenko 2017.
 *
 * Distributed under the MIT License.
 * (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT)
 *******************************************************************************
 */

#include <Arduino.h>
#include <string.h>
#include <ESP8266WiFi.h>
#include "wifi_config.h"

#define INTERVAL(start,end) (int32_t)((end<start) ? (0-start+end) : (end-start))
#define HB_SPEED 100
#define BTN_WAIT 1000
#define MSG_DELAY 60000

uint8_t cos_tbl[36] = {
  255,253,247,238,225,209,191,171,150,128,
  105, 84, 64, 46, 30, 17,  8,  2,  0,  2,
    8, 17, 30, 46, 64, 84,105,128,150,171,
  191,209,225,238,247,253
};

enum {
  RELAY_ON = LOW,
  RELAY_OFF = HIGH
};

enum {
  LED_OFF = 0,
  LED_ON,
  LED_HEARTBEAT
};

uint8_t led_state = LED_HEARTBEAT;
uint8_t led_intensity = 0;
uint8_t led_pos = 0;
uint32_t last_led_chg = 0;
uint32_t last_btn_chg = 0;
uint32_t last_message = 0;
uint8_t relay_state = RELAY_OFF;

// Enable MqttClient logs
#define MQTT_LOG_ENABLED 1
// Include library
#include <MqttClient.h>


#define BTN_LED 13
#define BTN 12
#define RELAY 14

byte mac[6];
char client_id[] = "esp_000000000000";

#define LOG_PRINTFLN(fmt, ...)	logfln(fmt, ##__VA_ARGS__)
#define LOG_SIZE_MAX 128
void logfln(const char *fmt, ...) {
	char buf[LOG_SIZE_MAX];
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, LOG_SIZE_MAX, fmt, ap);
	va_end(ap);
	Serial.println(buf);
}

#define HW_UART_SPEED									115200L
#define MQTT_ID											"TEST-ID"

const char* MQTT_TOPIC_SUB = "test/esp_000000000000/sub"; // Direct command channel
char* MQTT_TOPIC_PUB = "nodes/esp_000000000000/pub"; // Direct node channel

static MqttClient *mqtt = NULL;
static WiFiClient network;

char* getMAC() {
  WiFi.macAddress(mac);
  sprintf(
    client_id,
    "esp_%02x%02x%02x%02x%02x%02x",
    mac[0],
    mac[1],
    mac[2],
    mac[3],
    mac[4],
    mac[5]);
  return client_id;
}

char* getPubTopic() {
  WiFi.macAddress(mac);
  sprintf(
    MQTT_TOPIC_PUB,
    "nodes/esp_%02x%02x%02x%02x%02x%02x/pub",
    mac[0],
    mac[1],
    mac[2],
    mac[3],
    mac[4],
    mac[5]);
  return MQTT_TOPIC_PUB;
}

// ============== Object to supply system functions ============================
class System: public MqttClient::System {
public:

	unsigned long millis() const {
		return ::millis();
	}

	void yield(void) {
		::yield();
	}
};

// ============== Setup all objects ============================================
void setup() {
	// Setup hardware serial for logging
	Serial.begin(HW_UART_SPEED);
	//while (!Serial);

  
  pinMode(BTN_LED, OUTPUT);
  pinMode(BTN, INPUT_PULLUP);
  pinMode(RELAY, OUTPUT);

  digitalWrite(RELAY, relay_state);
  
	// Setup WiFi network
	WiFi.mode(WIFI_STA);
	WiFi.hostname(getMAC());
	WiFi.begin(STASSID, STAPSK);
	LOG_PRINTFLN("\n");
	LOG_PRINTFLN("Connecting to WiFi");
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		LOG_PRINTFLN(".");
	}
	LOG_PRINTFLN("Connected to WiFi");
	LOG_PRINTFLN("IP: %s", WiFi.localIP().toString().c_str());

	// Setup MqttClient
	MqttClient::System *mqttSystem = new System;
	MqttClient::Logger *mqttLogger = new MqttClient::LoggerImpl<HardwareSerial>(Serial);
	MqttClient::Network * mqttNetwork = new MqttClient::NetworkClientImpl<WiFiClient>(network, *mqttSystem);
	//// Make 128 bytes send buffer
	MqttClient::Buffer *mqttSendBuffer = new MqttClient::ArrayBuffer<128>();
	//// Make 128 bytes receive buffer
	MqttClient::Buffer *mqttRecvBuffer = new MqttClient::ArrayBuffer<128>();
	//// Allow up to 2 subscriptions simultaneously
	MqttClient::MessageHandlers *mqttMessageHandlers = new MqttClient::MessageHandlersImpl<2>();
	//// Configure client options
	MqttClient::Options mqttOptions;
	////// Set command timeout to 10 seconds
	mqttOptions.commandTimeoutMs = 10000;
	//// Make client object
	mqtt = new MqttClient(
		mqttOptions, *mqttLogger, *mqttSystem, *mqttNetwork, *mqttSendBuffer,
		*mqttRecvBuffer, *mqttMessageHandlers
	);
}

// ============== Main loop ====================================================

void loop() {
    
	// Check connection status
	if (!mqtt->isConnected()) {
		// Close connection if exists
		network.stop();
		// Re-establish TCP connection with MQTT broker
		LOG_PRINTFLN("Connecting");
		network.connect(MQTT_SERVER, 1883);
		if (!network.connected()) {
			LOG_PRINTFLN("Can't establish the TCP connection");
			delay(5000);
			return;
		}
		// Start new MQTT connection
		MqttClient::ConnectResult connectResult;
		// Connect
		{
			MQTTPacket_connectData options = MQTTPacket_connectData_initializer;
			options.MQTTVersion = 4;
			options.clientID.cstring = getMAC();
			options.cleansession = true;
			options.keepAliveInterval = 90; // 15 seconds
			MqttClient::Error::type rc = mqtt->connect(options, connectResult);
			if (rc != MqttClient::Error::SUCCESS) {
				LOG_PRINTFLN("Connection error: %i", rc);
				return;
			}
		}
		{
			// Add subscribe here if required
		}
	} else {
    {
      if(INTERVAL(last_message,millis())> MSG_DELAY) {
        // Add publish here if required
        String json_data = "{\"switch\":";
        json_data += (relay_state==RELAY_ON) ? 1 : 0;
        json_data += "}";
        
        MqttClient::Message message;
        message.qos = MqttClient::QOS0;
        message.retained = false;
        message.dup = false;
        message.payload = (void*) json_data.c_str();
        message.payloadLen = json_data.length();
        mqtt->publish(getPubTopic(), message);
        last_message = millis();
      }
    }
  }

  if(digitalRead(BTN)==0 && (INTERVAL(last_btn_chg,millis()) > BTN_WAIT)) {
    switch(relay_state) {
      case RELAY_OFF:
        Serial.println("Mode set to ON");
        led_state = LED_ON;
        relay_state = RELAY_ON;
        digitalWrite(RELAY, RELAY_ON);
      break;
      case RELAY_ON:
        Serial.println("Mode set to HEARTBEAT");
        led_state = LED_HEARTBEAT;
        relay_state = RELAY_OFF;
        digitalWrite(RELAY, RELAY_OFF);
      break;
    }
    last_btn_chg = millis();
  }

  // Changes LED visualization
  switch(led_state) {
    case LED_OFF:
      analogWrite(BTN_LED, 0);
      break;
    case LED_ON:
      analogWrite(BTN_LED, 255);
      break;
    case LED_HEARTBEAT:
      if(INTERVAL(last_led_chg,millis()) > HB_SPEED) {
        led_pos = (led_pos>=36) ? 0 : led_pos;
        analogWrite(BTN_LED, cos_tbl[led_pos++]);
        last_led_chg = millis();
      }
      break;
  }
}
