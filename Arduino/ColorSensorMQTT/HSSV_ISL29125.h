#ifndef HSSV_ISL29125_H
#define HSSV_ISL29125_H

/* ISL29125 Datasheet page 7 */
#define ISL29125_ADDRESS        0b01000100

/* Default device ID */
#define ISL29125_DEFAULT_DEVICE_ID 0x7D

/* ISL29125 Datasheet page 9 */
#define ISL29125_DEVICE_ID      0x00
#define ISL29125_DEVICE_RESET   0x00
#define ISL29125_CONFIG_1       0x01
#define ISL29125_CONFIG_2       0x02
#define ISL29125_CONFIG_3       0x03
#define ISL29125_TH_LOW_LSB     0x04 // B is for Byte
#define ISL29125_TH_LOW_MSB     0x05 // B is for Byte
#define ISL29125_TH_HIGH_LSB    0x06 // B is for Byte
#define ISL29125_TH_HIGH_MSB    0x07 // B is for Byte
#define ISL29125_STATUS         0x08
#define ISL29125_GRN_LSB        0x09
#define ISL29125_GRN_MSB        0x0A
#define ISL29125_RED_LSB        0x0B
#define ISL29125_RED_MSB        0x0C
#define ISL29125_BLU_LSB        0x0D
#define ISL29125_BLU_MSB        0x0E

/* BIT OFFSET */
#define CONFIG_1_MODE      0x00
#define CONFIG_1_RNG       0x03
#define CONFIG_1_RES       0x04
#define CONFIG_1_SYNC      0x05
#define FLAG_CONV_COMPLETE 0x01

/* Class definition for the ISL29125 Color Sensor*/
class HSSV_ISL29125 {
public:
  HSSV_ISL29125();

  uint8_t getDeviceId();
  uint8_t readConfig1();
  void reset();
  void begin();
  void updateRGB();
  uint16_t getRed();
  uint16_t getGreen();
  uint16_t getBlue();

private:
  uint8_t conversionIsComplete();
  void clearConversionComplete();
  void writeReg(uint8_t address,uint8_t data);
  uint8_t readReg(uint8_t address);
  uint8_t rx_tx_buffer[2];
  uint8_t device_id = 0;
  uint8_t enabled = 0;
  uint16_t red = 0;
  uint16_t green = 0;
  uint16_t blue = 0;
};

#endif
