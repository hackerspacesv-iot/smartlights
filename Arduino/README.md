# Sketchs de Arduino
Este directorio contiene los sketchs de Arduino que permiten al sistema
funcionar.

* ColorSensorWiFi: Código para sensor de color con micro-servidor web.
* ColorSensorMQTT: Código para sensor de color con publicación MQTT.
* SmartSwitchWiFi: Código de interruptor con micro-servidor web.
* SmartSwitchMQTT: Código de interruptor con publicación MQTT.

Nota: En todos los ejemplos es necesario renombrar el archivo
wifi_config_example.h a wifi_config.h y llenar con los parámetros
requeridos de red WiFi.

## Temas de publicación sobre MQTT
Para el caso de publicación de nodos MQTT los nodos publican al tema:

    nodes/dev-hhhhhhhhhhhh/pub

Donde "hhhhhhhhhhhh" corresponde a la dirección MAC del nodo.
