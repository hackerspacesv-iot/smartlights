/*
 *  SmartSwitchWiFi is part of Hackerspace San Salvador - SmartLights
 *  
 *  Copyright 2019 - Mario Gómez @ Hackerspace San Salvador
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "wifi_config.h"

const char* ssid = STASSID;
const char* password = STAPSK;

#define BTN_LED 13
#define BTN 12
#define RELAY 14

#define HB_SPEED 100
#define BTN_WAIT 1000

uint8_t cos_tbl[36] = {
  255,253,247,238,225,209,191,171,150,128,
  105, 84, 64, 46, 30, 17,  8,  2,  0,  2,
    8, 17, 30, 46, 64, 84,105,128,150,171,
  191,209,225,238,247,253
};

enum {
  RELAY_ON = LOW,
  RELAY_OFF = HIGH
};

enum {
  LED_OFF = 0,
  LED_ON,
  LED_HEARTBEAT
};

uint8_t led_state = LED_HEARTBEAT;
uint8_t led_intensity = 0;
uint8_t led_pos = 0;
uint32_t last_led_chg = 0;
uint32_t last_btn_chg = 0;
uint8_t relay_state = RELAY_OFF;

ESP8266WebServer server(80);

void handleRoot() {
  server.send(200, "text/plain", "HackerspaceSV IoT Switch");
}

void handleSensor() {
  String message = "{\"switch\":";
  message += (relay_state==RELAY_ON) ? 1 : 0;
  message += "}";
  server.send(200, "application/json", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  
  pinMode(BTN_LED, OUTPUT);
  pinMode(BTN, INPUT_PULLUP);
  pinMode(RELAY, OUTPUT);

  digitalWrite(RELAY, relay_state);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/sensor", handleSensor);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
  MDNS.update();

  if(digitalRead(BTN)==0 && (millis()-last_btn_chg > BTN_WAIT)) {
    switch(relay_state) {
      case RELAY_OFF:
        led_state = LED_ON;
        relay_state = RELAY_ON;
        digitalWrite(RELAY, RELAY_ON);
      break;
      case RELAY_ON:
        led_state = LED_HEARTBEAT;
        relay_state = RELAY_OFF;
        digitalWrite(RELAY, RELAY_OFF);
      break;
    }
    last_btn_chg = millis();
  }

  // Changes LED visualization
  switch(led_state) {
    case LED_OFF:
      analogWrite(BTN_LED, 0);
      break;
    case LED_ON:
      analogWrite(BTN_LED, 255);
      break;
    case LED_HEARTBEAT:
      if(millis()-last_led_chg > HB_SPEED) {
        led_pos = (led_pos>=36) ? 0 : led_pos;
        analogWrite(BTN_LED, cos_tbl[led_pos++]);
        last_led_chg = millis();
      }
      break;
  }
}
