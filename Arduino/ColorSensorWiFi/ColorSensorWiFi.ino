/*
 *  ColorSensorWiFi is part of Hackerspace San Salvador - SmartLights
 *  
 *  Copyright 2019 - Mario Gómez <mario.gomez@teubi.co> @ Hackerspace San Salvador
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
//#include <Wire.h>
#include "HSSV_ISL29125.h"
#include "wifi_config.h"

#define INTERVAL(start,end) (int32_t)((end<start) ? (0-start+end) : (end-start))
#define RGB_UPDATE_INTERVAL 10000

// Declare sensor object
HSSV_ISL29125 RGB_sensor;

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

const int led = LED_BUILTIN;

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "Hackerspace IoT ISL29125");
  digitalWrite(led, 0);
}

void handleSensor() {
  String message = "{\"r\":";
  message += RGB_sensor.getRed();
  message += ",\"g\":";
  message += RGB_sensor.getGreen();
  message += ",\"b\":";
  message += RGB_sensor.getBlue();
  message += "}";
  server.send(200, "application/json", message);
}

void handleNotFound() {
  String message = "Archivo no encontrado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  RGB_sensor.begin();
  
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  
  server.on("/sensor", handleSensor);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

uint32_t last_update = 0;

void loop(void) {
  server.handleClient();
  MDNS.update();
  
  if (INTERVAL(last_update,millis()) > RGB_UPDATE_INTERVAL) {
    digitalWrite(led, HIGH);
    Serial.println("Updating RGB values");
    RGB_sensor.updateRGB();
    digitalWrite(led, LOW);
    last_update = millis();
  }
}
