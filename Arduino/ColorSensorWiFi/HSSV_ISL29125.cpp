#include <stdint.h>
#include <Arduino.h>
#include <Wire.h>
#include "brzo_i2c.h"
#include "HSSV_ISL29125.h"

/* Resets the Sensor and sets the 
 * default configuration 
 */
HSSV_ISL29125::HSSV_ISL29125() {
  brzo_i2c_setup(12, 14, 2000);
}

/*
 * Reads the specified address from the device
 */
void HSSV_ISL29125::writeReg(uint8_t address,uint8_t data) {
  uint8_t bcode = 0;
  if(enabled==1) {
    Wire.beginTransmission(ISL29125_ADDRESS);
    Wire.write(address);
    Wire.write(data);
    Wire.endTransmission();
  }
}

/*
 * Reads the specified address from the device
 */
uint8_t HSSV_ISL29125::readReg(uint8_t address) {
  uint8_t bcode = 0;
  uint8_t value = 0;

  if(enabled==1) {
    Wire.beginTransmission(ISL29125_ADDRESS);
    Wire.write(address);
    Wire.endTransmission();

    Wire.requestFrom(ISL29125_ADDRESS, 1);
  
    if(1 <= Wire.available()) {
      value = Wire.read();
    }
  }

  return value;
}

/*
 * Restarts the device to the default values
 */
void HSSV_ISL29125::reset() {
  writeReg(ISL29125_DEVICE_RESET,0x00);
}

/*
 * Sets default device configuration 
 */
void HSSV_ISL29125::begin() {
  uint8_t reg_val;
  Wire.begin(12,14);

  enabled = 1;
  this->reset();
  device_id = this->readReg(ISL29125_DEVICE_ID);
  enabled = 0;
  
  if(device_id == ISL29125_DEFAULT_DEVICE_ID) {
    enabled = 1;
  }
  
  reg_val = readReg(ISL29125_CONFIG_1);

  reg_val = (reg_val & 0b11111000) | (0b101); // RGB ADC ON
  reg_val = (reg_val & 0b11110111) | (0b1<<CONFIG_1_RNG); // 16 bit range
  reg_val = (reg_val & 0b11101111) | (0b0<<CONFIG_1_RES); // 16 bit range
  reg_val = (reg_val & 0b11011111) | (0b0<<CONFIG_1_SYNC); // 16 bit range

  writeReg(ISL29125_CONFIG_1,reg_val);
}

/*
 * Reads sensor configuration 1
 */
uint8_t HSSV_ISL29125::readConfig1() {
  return readReg(ISL29125_CONFIG_1);
}

/*
 * Returns internal green value
 */
uint16_t HSSV_ISL29125::getGreen() {
  return green;
}

/*
 * Returns internal red value
 */
uint16_t HSSV_ISL29125::getRed() {
  return red;
}

/*
 * Returns internal blue value
 */
uint16_t HSSV_ISL29125::getBlue() {
  return blue;
}

/*
 * Reads RGB ADC data
 */
void HSSV_ISL29125::updateRGB() {
  if(conversionIsComplete()) {
    red = readReg(ISL29125_RED_LSB);
    red |= readReg(ISL29125_RED_MSB)<<8;
    green = readReg(ISL29125_GRN_LSB);
    green |= readReg(ISL29125_GRN_MSB)<<8;
    blue = readReg(ISL29125_BLU_LSB);
    blue |= readReg(ISL29125_BLU_MSB)<<8;
    clearConversionComplete();
  }
}

/*
 * This method returns zero if 
 * conversion is not complete and one
 * otherwise.
 */
uint8_t HSSV_ISL29125::conversionIsComplete() {
  uint8_t reg_val = readReg(ISL29125_STATUS);

  reg_val = (reg_val>>FLAG_CONV_COMPLETE) & 0x01;

  return reg_val;
}

/*
 * This method clears the conversion complete flag
 */
void HSSV_ISL29125::clearConversionComplete() {
  uint8_t reg_val = readReg(ISL29125_STATUS);
  reg_val &= 0b11111101;
  writeReg(ISL29125_STATUS, reg_val);
}

/*
 * Returns the device id from the device.
 */
uint8_t HSSV_ISL29125::getDeviceId() {
  return device_id;
}
