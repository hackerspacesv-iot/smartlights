# Configuración de iptables
Este directorio contiene los scripts de configuracion utilizados
para el monitoreo de actividad de red.

Estos scripts solo funcionarán en una máquina Linux que esté
funcionando como Gateway a Internet en la red donde se pretenda
implementar este proyecto.

Se incluyen dos versiones de los scripts tanto para IPv4 como
para IPv6.

Edite los archivos y cambie las variables de entorno a donde se
indique según la configuración de su Gateway.

Nota: Estos scripts deben ser ejecutados como root. Se asume
que no existen otras reglas de iptables. Si existen otras reglas
asegurese de que estas sean agregadas primero para que los
paquetes sean marcados de forma adecuada.
