#!/bin/bash

# Especificar la interfaz de entrada
IN_IF=eth0
# Especificar la interfaz de salida
OUT_IF=eth3
# Especificar la IP inicial a monitorear
START_IP=192.168.1.101
# Especificar la IP final a monitorear
END_IP=192.168.1.250

iptables -t mangle -A FORWARD -i $IN_IF -o $OUT_IF -p tcp --dport 80 -m iprange --src-range $START_IP-$END_IP -j MARK --set-mark 0x08
iptables -t mangle -A FORWARD -i $IN_IF -o $OUT_IF -p tcp --dport 443 -m iprange --src-range $START_IP-$END_IP -j MARK --set-mark 0x08
