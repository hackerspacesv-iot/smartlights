#!/bin/bash

# Especificar la interfaz de entrada
IN_IF=eth0
# Especificar la interfaz de salida
OUT_IF=he-tun0

ip6tables -t mangle -A FORWARD -i $IN_IF -o $OUT_IF -p tcp --dport 80 -j MARK --set-mark 0x08
ip6tables -t mangle -A FORWARD -i $IN_IF -o $OUT_IF -p tcp --dport 443 -j MARK --set-mark 0x08
