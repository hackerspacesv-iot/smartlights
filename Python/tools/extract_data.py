from datetime import datetime
from datetime import timedelta
import MySQLdb as mariadb

# Fecha de Inicio de Extracción
datetime_start = datetime(2019, 4, 12, 0, 0, 0, 0)
# Fecha de Final de Extracción
datetime_end = datetime(2019, 4 , 17, 23, 59, 59, 0)

db = mariadb.connect('localhost','usuario','password','database')
cursor = db.cursor()

# Print CSV Header
print("start,end,r,g,b,switch")

while datetime_start < datetime_end:
  start_interval = datetime_start.strftime("%Y-%m-%d %H:%M:%S")
  datetime_start = datetime_start + timedelta(seconds=60)
  end_interval = datetime_start.strftime("%Y-%m-%d %H:%M:%S")
  
  output = start_interval+","+end_interval+","

  result = cursor.execute("SELECT r,g,b FROM `light_sensor` WHERE timestamp BETWEEN %s AND %s",(start_interval,end_interval))

  c = 0
  sum_r = 0
  sum_g = 0
  sum_b = 0
  for r,g,b in cursor:
    sum_r += r
    sum_g += g
    sum_b += b
    c += 1   

  if c > 0:
    output += str(int(sum_r/c))+","+str(int(sum_g/c))+","+str(int(sum_b/c))+","
  else:
    output += ",,,"

  result = cursor.execute("SELECT `val` FROM `switch_sensor` WHERE `captured` BETWEEN %s AND %s",(start_interval,end_interval))

  c = 0
  v = 0
  for val in cursor:
    v = val
    c += 1
    break

  if c > 0:
    output += str(v[0])

  print(output)

db.close()

