# Scripts de recolección y almacenamiento de dato

Los Scripts de python incluídos en esta carpeta se encargan
de conectarse a los sensores IoT, descargar la información acerca
de su estado en formato JSON y almacenar el resultado en
la base de datos MariaDB.

Los archivos se programan para ejecutarse de forma regular
utilizando una tarea programada de CRON.

Se incluyen tres scripts de Python:

1. storecolor: Guarda la información de color del sensor de luz.
2. storeswitch: Guarda el estado del switch (encendido/apagado).
3. storepackets: Guarda los conteos de paquetes que atraviesan el servidor.

Los scripts storecolor y storeswitch incluyen una versión adicional
para ser utilizada con el protocolo MQTT. Estas versiones corren como
tarea en segundo plano a menos que se llamen con parámetro -f.

