#!/usr/bin/python
import urllib
import requests
import json
import MySQLdb as mariadb

# Agrega la dirección del dispositivo
deviceAddr = "192.168.1.88"
service = "sensor"

srvURL = "http://"+deviceAddr+"/"+service

# Agrega la información de la base de datos
db = mariadb.connect('localhost','usuario','password','database')
cursor = db.cursor()

#Do request
print "Trying to download data"
response = None
data = ""

try:
  response = requests.get(srvURL, timeout=10);
except:
  print "Cannot get URL"
  raise

try:
  data = json.loads(response.content)
except:
  print "Ups. We got an error downloading data. Check connections"
  raise

print str(data)
switch = data['switch']

print "Data downloaded successfully!"
print "Valor: "+str(switch)

try:
  cursor.execute("INSERT INTO switch_sensor(val) VALUES(%d)" % (switch))
  db.commit()
  print "Record stored in database"
except:
  print "Ups. We got an error trying to store the data."
  raise

db.close()

