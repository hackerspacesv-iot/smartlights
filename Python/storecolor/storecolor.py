#!/usr/bin/python
import urllib
import requests
import json
import MySQLdb as mariadb

# Agrega la dirección del dispositivo en la siguiente linea
deviceAddr = ""
service = "sensor"

srvURL = "http://"+deviceAddr+"/"+service

# Agrega la información de conexión de tu base de datos
db = mariadb.connect('localhost','user','password','database')
cursor = db.cursor()

#Do request
print "Trying to download data"
response = None
data = ""

try:
  response = requests.get(srvURL, timeout=10);
except:
  print "Cannot get URL"
  raise

try:
  data = json.loads(response.content)
except:
  print "Ups. We got an error downloading data. Check connections"
  raise

print str(data)
red = data['r']
green = data['g']
blue = data['b']

print "Data downloaded successfully!"
print "Red: "+str(red)
print "Green: "+str(green)
print "Blue: "+str(blue)

try:
  cursor.execute("INSERT INTO light_sensor(r,g,b) VALUES(%d,%d,%d)" % (red,green,blue))
  db.commit()
  print "Record stored in database"
except:
  print "Ups. We got an error trying to store the data."
  raise

db.close()

