#!/usr/bin/python
import signal
import sys
import time
import json
import threading
import os
import paho.mqtt.client as mqtt
import MySQLdb as mariadb
import local_config as config
import subprocess
from threading import Thread

class ColorSensorMQTTSubscriber(Thread):

  def __init__(self,db,server,topic,port=1883,keepalive=60):
    Thread.__init__(self)
    self.db = db
    self.cursor = db.cursor()

    self.continue_process = True
    self.server = server
    self.topic = topic
    self.port = port
    self.keepalive = keepalive

  def stop(self):
    global continue_process
    self.client.loop_stop()
    self.continue_process = False

  def on_connect(self, client, userdata, flags, rc):
    print("Connected with result code"+str(rc))

    self.client.subscribe(self.topic)

  def on_message(self, client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    data = json.loads(msg.payload)
    red = data['r']
    green = data['g']
    blue = data['b']
    print("Storing data (R,G,B) <= (%d,%d,%d)" % (red,green,blue))
    self.cursor.execute("INSERT INTO light_sensor(r,g,b) VALUES(%d,%d,%d)" % (red,green,blue))
    self.db.commit()
 
  def run(self):
    self.client = mqtt.Client()
    self.client.on_connect = self.on_connect
    self.client.on_message = self.on_message

    self.client.connect(self.server,self.port,self.keepalive)
    self.client.loop_start()

    while self.continue_process:
      time.sleep(0.1)

    print("Ending process...")
    self.db.close()
    print("Database closed...")

db = mariadb.connect(config.db_srv,config.db_usr,config.db_psw,config.db_dbn)
colorSensorClient = ColorSensorMQTTSubscriber(db,config.server,config.topic)

if __name__ == '__main__':
  if len(sys.argv)>1 and sys.argv[1] == '-f':
    colorSensorClient.start()
    colorSensorClient.join()
    sys.exit(0)
  else:
    subprocess.Popen(['./storecolor_mqtt.py','-f'])

