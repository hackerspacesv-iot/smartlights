#!/usr/bin/python

import re
import subprocess
import MySQLdb as mariadb

db = mariadb.connect('localhost','username','password','database')
cursor = db.cursor()

output = subprocess.check_output(['iptables','-t','mangle','-vv','-L','-n'])
output.split('\n')
c = 0
lines = output.split('\n')
for line in lines:
  if c == 50:
    prog  = re.compile("Counters: ([0-9]+) packets, [0-9]+ bytes")
    match = prog.match(line)
    packets = int(match.group(1))
    cursor.execute("INSERT INTO packet_counter(packets) VALUES(%d)" % (packets,))
    db.commit()
  c += 1

db.close()

