# Hackerspace San Salvador - SmartLights

Desde la primera casa del Hackerspace San Salvador hemos querido
automatizar las luces del espacio. Así que decidimos que casi tres
años después de haber inaugurado el espacio no podíamos seguir
postponiendo este proyecto.

## ¿Cuál es el objetivo de este proyecto?

El objetivo de las SmartLights es automatizar las luces a traves del
análisis de la iluminación del recinto y de lo que los usuarios del
espacio consideran como "iluminación adecuada".

En lugar de definir un punto de corte fijo para encender las luces,
esperamos generar un modelo basado en datos que aprenda automáticamente
cuándo es apropiado encender las luces del hackerspace.

## ¿Qué datos pretendemos usar?

Este proyecto pretende generar un modelo para iluminación óptima para
el hackerspace a partir de los siguientes parámetros primarios:

* Cantidad de iluminación natural del espacio
* Patron de activación manual de las luces por los usuarios
* Actividad de red local (para asegurarse de que no se enciendan si no
hay usuarios)

Se pretende también incluir otros parámetros derivados de fuentes
secundarias que podrían incluír pero no limitarse a:

* Presencia e intensidad de cobertura nubosa (Derivado de imágenes de
satélite)
* Elevación del sol (Derivado de la Hora y Fecha)

Los datos primarios serán recolectados a través de sensores IoT conectados
a la red local y se almacenarán en el servidor para su análisis y
generación del modelo.

## Etapas del proyecto

Este proyecto será dividido en tres etapas:

1. Creación de Hardware (Sensor de Iluminación e Interruptor Administrado)
2. Captura de datos (con al menos un mes de datos)
3. Generación del modelo automatizado

Los dispositivos de la etapa 1 serán utilizados para monitorear la cantidad
de luz en el espacio como también el momento en que los usuarios activan la
iluminación artificial.

Los datos de activación manual serán utilizados como objetivo de aprendizaje
del modelo para garantizar que el comportamiento autónomo sea lo más parecido
posible a las necesidades reales de los usuarios del espacio.

## Estructura del Prototipo
El siguiente diagrama resume la estructura actual de cómo se ha implementado
todo el prototipo en su primera etapa:

![Estructura de red](./Docu/Estructura.png "Diagrama de Estructura de Red")

El diagrama corresponde a la segunda iteración de la etapa 1. En esta etapa
se utiliza un servidor MQTT (Eclipse-Mosquitto) para centralizar la captura
de eventos generados por los dispositivos de medición de luz y el switch
administrado.

Los mensajes publicados al servidor MQTT son luego capturados por dos
scripts de Python suscritos a las notificaciones. Estos scripts guardan de
forma persistente los valores capturados en un servidor MySQL.

La actividad de red es monitoreada marcando los paquetes con destino a
protocolos IPv4 e IPv6 dentro de la red. Luego un script de Python consulta
de forma regular los conteos de paquetes y almacena el resultado en MySQL.

## Software Mínimo Requerido
Este proyecto necesita del siguiente software para funcionar:

* Servidor MySQL
* Servidor MQTT (Eclipse-Mosquitto)
* Soporte de iptables para marcado de paquetes (MARK)
* Python

### Requerimientos de Python
Los scripts de Python de este proyecto requieren adicionalmente el soporte
de las siguientes bibliotecas:

* MySQLdb
* PAHO-MQTT

El script que lee los conteos de paquetes de iptables (y el propio iptables)
requieren permisos de administrador para poder ser utilizados.

## Acerca de este repositorio

Este repositorio contiene el código fuente de los sensores de prototipo
y esperamos tener las versiones finales disponibles una vez se estables.

El código fuente incluye una serie de scripts y código utilizado para
captura de datos. El código inicialmente se realiza en Arduino para los
prototipos por su facilidad de uso, pero se pretente migrar posteriormente
a un Sistema Operativo de IoT como RIoT.

## Licencia
Todo el código fuente incluído en este repositorio está cubierto bajo
la licencia GNU/GPL v3.0 a menos que se especifique lo contrario en
los archivos de código fuente.

    Copyright 2019 - Mario Gómez @ Hackerspace San Salvador

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

