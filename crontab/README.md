# Tareas programadas de CRON
En el archivo cron.txt puedes encontrar dos líneas de comando
de ejemplo para automatizar la ejecución de los scripts de
captura y almacenamiento de datos.

Desde la consola ejecuta el comando crontab -e para agregar
las dos líneas contenidas en el archivo. Asegúrate de escribir
la ruta completa a los scripts correspondientes
